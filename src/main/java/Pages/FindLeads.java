package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectsMethods;

public class FindLeads extends ProjectsMethods
{
	public FindLeads()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using ="//input[@name='firstName']")
	WebElement eletypefirstname;
	
	@FindBy(how=How.XPATH, using ="//button[text()='Find Leads']")
	WebElement eleclickfindlead;
	
	
	public FindLeads typeFirstName(String enterfirstname) throws InterruptedException
	
	{
		Thread.sleep(3000);
		type(eletypefirstname, enterfirstname);
		return this;
	}
	
	public FindLeads clickFindButton()
	
	{
		click(eleclickfindlead);
		return this;
	}
	
}