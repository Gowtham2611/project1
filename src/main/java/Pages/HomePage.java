package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectsMethods;

public class HomePage extends ProjectsMethods
{
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
		
	@FindBy(how = How.XPATH, using= "//div[@class=\"insideHeaderText\"]/a[3]" )
	WebElement eleclickLogoff;
	
	@FindBy(how = How.LINK_TEXT, using="CRM/SFA" )
	WebElement eleclickcrmsfa;
	
	
	public LoginPage clickLogoff()
	{
		click(eleclickLogoff);
		return new LoginPage();
	}
	
	public MyHomePage clickcrmsfa()
	{
		click(eleclickcrmsfa);
		return new MyHomePage();
	}
	
}
