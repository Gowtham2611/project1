package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectsMethods;

public class ViewLead extends ProjectsMethods
{

	public ViewLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="viewLead_firstName_sp")
	WebElement viewfirstname;
	
	public void validatefirstname()
	{
		System.out.println(viewfirstname.getText());
	}
}
