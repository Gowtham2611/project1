package Pages;

import java.awt.List;
import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectsMethods;

public class MergeLead extends ProjectsMethods
{

	
	public MergeLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	
	@FindBy(how=How.XPATH, using="//img[@alt='Lookup']")
	WebElement eleselectfromlead;
	
		
	public FindLeads selectFromLead() throws InterruptedException
	
	{
		click(eleselectfromlead);
		
		Thread.sleep(3000);
		switchToWindow(2);
		
		return new FindLeads();
		
	}
}
