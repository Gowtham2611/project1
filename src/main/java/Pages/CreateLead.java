package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectsMethods;

public class CreateLead extends ProjectsMethods
{
	
	public CreateLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") 
	WebElement eleentercompanyname;
	
	@FindBy(how=How.ID, using="createLeadForm_firstName")
	WebElement eleenterfirstname;
	
	@FindBy(how=How.ID, using="createLeadForm_lastName")
	WebElement eleenterlastname;
	
	@FindBy(how = How.CLASS_NAME, using ="smallSubmit")
	WebElement clickcreatelead;
	
	
	
	public CreateLead enterCompanyname(String companyname)
	{
		type(eleentercompanyname, companyname);
		return this;
	}
	
	
	public CreateLead enterfirstname(String firstname)
	{
		type(eleenterfirstname, firstname);
		return this;
	}
	
	
	public CreateLead enterLastname(String lastname)
	{
		type(eleenterlastname, lastname);
		return this;
	}
	
	
	public ViewLead clickCreateLead()
	{
		click(clickcreatelead);
		return new ViewLead();
	}
	
}
