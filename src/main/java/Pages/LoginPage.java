package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectsMethods;

public class LoginPage extends ProjectsMethods
{

		
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID , using="username")
	WebElement eleusername;
	
	@FindBy(how = How.ID , using ="password")
	WebElement elepassword;
	
	@FindBy(how = How.CLASS_NAME, using= "decorativeSubmit" )
	WebElement eleclickLogin;
	
	public LoginPage enterUsername(String uName)
	{
		
		type(eleusername, uName);
		return this;
		
	}
	
	public LoginPage enterPassword(String password)
	{
		type(elepassword, password);
		return this;
	}
	
	public HomePage clickLogin()
	{
		click(eleclickLogin);
		return new HomePage();
	}
	
	
	
}
