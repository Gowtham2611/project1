package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectsMethods;

public class MyHomePage extends ProjectsMethods
{
	
	public MyHomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how = How.XPATH , using="//a[text()='Leads']")
	WebElement eleclicklead;
	
	
	
	
	public MyLead Leads()
	{
		click(eleclicklead);
		
		return new MyLead();
	}
}
