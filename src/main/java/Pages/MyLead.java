package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectsMethods;

public class MyLead extends ProjectsMethods
{
	public MyLead()
	{
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(how=How.LINK_TEXT, using="Create Lead")
	WebElement eleclickcreatelead;
	
	@FindBy(how = How.LINK_TEXT, using = "Merge Leads")
	WebElement eleclickmergelead;
	
	
	
	public CreateLead createLeadLink()
	{
		click(eleclickcreatelead);
		return new CreateLead();
	}
	
	public MergeLead mergeLeadLink() throws InterruptedException
	{
		click(eleclickmergelead);
		Thread.sleep(3000);
		return new MergeLead();
	}
}
