package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public  class ReadExcel {

	public static Object[][] readExcel(String datasheetName) throws IOException
	{
	
		//open the excel
		
		XSSFWorkbook wb = new XSSFWorkbook("./data/"+datasheetName+".xlsx");
		
		
		//go to sheet
		
		XSSFSheet sheet = wb.getSheetAt(0);
		
		int rowNum = sheet.getLastRowNum();	//to get the row count
		System.out.println(rowNum);
		
				
		int cellNum = sheet.getRow(0).getLastCellNum(); //column count
		System.out.println(cellNum);
		
		Object[][] data = new Object[rowNum][cellNum];
		 	
		for (int i = 1; i <= rowNum; i++) //iterate over row
		{
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < cellNum; j++)	//iterate over column 
			{
				XSSFCell cell = row.getCell(j);
				
				try 
				{
					String value = cell.getStringCellValue();
					System.out.println(value);
					data[i-1][j]= value;
					
				} 
				catch (NullPointerException e)
				
				{
					System.out.println("");
				}
				
			}
		}
		

		//close the excel
		wb.close();
		return data;
		
	}

}
