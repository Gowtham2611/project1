package utils;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ReportHome
{
	public ExtentReports extent;
	public String status,description;
	public ExtentTest logger;
	public String testName,desc;
	public String author;
	public String category;
	
	@BeforeSuite
	public void reportStart() throws IOException // from LearnReport.java reportStart() one time execution and hence used annotation @beforeSuite
	{
		
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/report.html");
		
		html.setAppendExisting(true);
		
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
	}
	
	public void reportStatus(String status, String description)
	//from LearnReport.java reportStatus() method is one time execution and hence used annotation @beforeSuite
	{
		
		if(status.equalsIgnoreCase("Pass"))
		{
		logger.log(Status.PASS, "Login successfully");
		}
		else if (status.equalsIgnoreCase("fail")) 
		{
			logger.log(Status.FAIL, "Login successfully");
		}			
	}
	
	@BeforeMethod
	public void reportCases()
	{
		//Declare this as @beforeMethod level since there might me many test cases to be executed ie multiple time
		//ExtentTest logger = extent.createTest("TC001CreateLeads", "Created Leads");
	
			extent.createTest(testName, desc);
			logger.assignAuthor(author);
			logger.assignCategory(category);
	}
	
	@AfterSuite
	
	public void end()
	{
		extent.flush();
	}
	
}
