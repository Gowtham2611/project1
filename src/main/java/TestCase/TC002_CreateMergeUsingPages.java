package TestCase;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.HomePage;
import Pages.LoginPage;
import wdMethods.ProjectsMethods;

public class TC002_CreateMergeUsingPages extends ProjectsMethods
{
	
	@BeforeTest
	public void setData()
	{
		testCaseName="Merge lead";
		testDesc="creating a Merge";
		category="smoke";
		author="Gowtham";
		datasheetName= "mergeLead";
	}
	
	
	
	
	@Test(dataProvider="fetchdata")
	public void createMergeLead(String uName, String password,String enterfirstname) throws InterruptedException 
			/*String companyname,
			String firstname,String lastname) throws InterruptedException 
			*/
			
	{
		new LoginPage()
		.enterUsername(uName)
		.enterPassword(password)
		.clickLogin()
		.clickcrmsfa()
		.Leads()
		.mergeLeadLink()
		.selectFromLead()
		.typeFirstName(enterfirstname)
		.clickFindButton();
		
		
/*		.createLeadLink()
		.enterCompanyname(companyname)
		.enterfirstname(firstname)
		.enterLastname(lastname)
		.clickCreateLead()
		.validatefirstname();
		new HomePage().clickLogoff();
*/	
		
	}
	
}
