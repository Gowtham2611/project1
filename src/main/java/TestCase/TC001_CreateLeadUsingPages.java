package TestCase;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.HomePage;
import Pages.LoginPage;
import wdMethods.ProjectsMethods;

public class TC001_CreateLeadUsingPages extends ProjectsMethods
{
	
	@BeforeTest
	public void setData()
	{
		testCaseName="create lead";
		testDesc="creating a lead";
		category="smoke";
		author="Gowtham";
		datasheetName= "createLead";
	}
	
	
	
	
	@Test(dataProvider="fetchdata")
	public void createLead(String uName, String password, String companyname,
			String firstname,String lastname)
	{
		new LoginPage().
		enterUsername(uName).
		enterPassword(password).
		clickLogin()
		.clickcrmsfa()
		.Leads()
		.createLeadLink()
		.enterCompanyname(companyname)
		.enterfirstname(firstname)
		.enterLastname(lastname)
		.clickCreateLead()
		.validatefirstname();
		new HomePage().clickLogoff();
		
	}
	
}
