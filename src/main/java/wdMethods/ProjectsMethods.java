package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utils.ReadExcel;


public class ProjectsMethods extends SeMethods
{
	
	public static String datasheetName;
	
	@BeforeMethod(groups= {"any"}) 
	
	
	//@Parameters({"url","username","password"})
	public void login(/*String url, String username, String password*/)
	
	{
		startApp("chrome","http://leaftaps.com/opentaps/control/main");
		
		
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		
		WebElement eleCrmSfa = locateElement("link", "CRM/SFA");
		eleCrmSfa.click();
		*/
	}
	
	/*@AfterMethod(groups= {"any"})
	
	public void close()
	{
		closeBrowser();
	}*/
	
	@DataProvider(name="fetchdata")
	public Object [] [] getData() throws IOException
	{
		ReadExcel rd = new ReadExcel();
		Object[][] sheet= rd.readExcel(datasheetName);
		return sheet;
	}
	
	
}