package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class ProjectMethodsClass extends SeMethodsClass
{
	
	@BeforeSuite
	public void beforeSuite() 
	{
		startResult();
	}
	
	@BeforeTest
	public void beforeTest() 
	{
		System.out.println("@BeforeTest");
	}
	@BeforeClass
	public void beforeClass()
	{
		System.out.println("@BeforeClass");
	}
	@BeforeMethod
	public void login() 
	{
		beforeMethod();
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement crm = locateElement("link", "CRM/SFA");
		click(crm);		
	}
	
	@AfterMethod
	public void closeApp() 
	{
		closeBrowser();
	}
	@AfterClass
	public void afterClass() 
	{
		System.out.println("@AfterClass");
	}
	@AfterTest
	public void afterTest() 
	{
		System.out.println("@AfterTest");
	}
	@AfterSuite
	public void afterSuite() 
	{
		endResult();
	}
	


}






